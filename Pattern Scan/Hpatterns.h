#pragma once

struct offsetData
{
	char *name;
	char *pattern;
	char *mask;
	char *module;
	int skipBytes;
	bool subtractModule;
	int bytes2Add;
};

namespace pussyexplorer {

	namespace netvars {
		offsetData m_bSpotted = { (char*)"m_bSpotted", (char*)"\x75\x15\x8B\x87\x00\x00\x00\x00", (char*)"xxxx????", (char*)"client_panorama.dll", 4, false };
		offsetData m_dwBoneMatrix = { (char*)"m_dwBoneMatrix ", (char*)"\x75\x15\x8B\x87\x00\x00\x00\x00", (char*)"xxxx????", (char*)"client_panorama.dll", 4, false};
		offsetData m_iCrosshairId = {(char*)"m_iCrosshairId", (char*)"\x73\x21\x8B\x81\x00\x00\x00\x00\x85\xC0\x75\x19", (char*)"xxxx????xxxx", (char*)"client_panorama.dll", 4, false};
		offsetData m_fFlags = { (char*)"m_fFlags", (char*)"\x80\xBE\x5E\x02\x00\x00\x00\x8B\x86\x00\x00\x00\x00\x75\x14", (char*)"xxxxxxxxx????xx", (char*)"client_panorama.dll", 9, false};
		offsetData m_iHealth = {(char*)"m_iHealth", (char*)"\x8B\x81\x00\x00\x00\x00\xC3\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\xCC\x55\x8B\xEC\x56\x8B\x75\x08\x57\x8B\xF9\x83\x7E\x14\x10\x73\x15\x8B\x46\x10\x83\xC0\x01\x74\x1B", (char*)"xx????xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", (char*)"client_panorama.dll", 2, false};
		offsetData m_iTeamNum = { (char*)"m_iTeamNum", (char*)"\xE9\x00\x00\x00\x00\xCC\xCC\xCC\xCC\xCC\x8B\x81\x00\x00\x00\x00", (char*)"x????xxxxxxx????", (char*)"client_panorama.dll", 12, false};
		offsetData m_lifeState = { (char*)"m_lifeState", (char*)"\x80\xB9\x00\x00\x00\x00\x00\x0F\x94\xC0", (char*)"xx?????xxx", (char*)"client_panorama.dll", 2, false};
		offsetData m_vecOrigin = { (char*)"m_vecOrigin", (char*)"\x89\x46\x48\x8B\x87\x00\x00\x00\x00", (char*)"xxxxx????", (char*)"client_panorama.dll", 5, false};
	}

	namespace signatures {
		offsetData dwClientState_ViewAngles = { (char*)"dwClientState_ViewAngles", (char*)"\x8B\x14\x85\x00\x00\x00\x00\x8B\x82\x00\x00\x00\x00", (char*)"xxx????xx????", (char*)"engine.dll", 9, false};
		offsetData dwEntityList = { (char*)"dwEntityList", (char*)"\x8B\xC1\xC1\xE0\x04\x8B\x88\x00\x00\x00\x00\x85\xC9\x74\x31", (char*)"xxxxxxx????xxxx", (char*)"client_panorama.dll", 7, true, 16};
		offsetData dwLocalPlayer = { (char*)"dwLocalPlayer", (char*)"\x8D\x44\x24\x34\x50\xFF\x56\x00\x8B\x0D\x00\x00\x00\x00\x81\xF9\x00\x00\x00\x00\x75\x18\xF3\x0F\x10\x05\x00\x00\x00\x00\xF3\x0F\x11\x44\x24\x10\x81\x74\x24\x00\x00\x00\x00\x00\xEB\x0B\x8B\x01\x8B\x40\x30\xFF\xD0\xD9\x5C\x24\x10\xF3\x0F\x10\x05\x00\x00\x00\x00\x0F\x2F\x44\x24\x5C\x77\x0E\x80\x7C\x24\x66\x00", (char*)"xxxxxxx?xx????xx????xxxxxx????xxxxxxxxx?????xxxxxxxxxxxxxxxxx????xxxxxxxxxxxx", (char*)"client_panorama.dll", 26, true, 48};
		offsetData m_bDormant = { (char*)"m_bDormant", (char*)"\x75\x0D\x8B\x86\x00\x00\x00\x00\xC1\xE8\x03", (char*)"xxxx????xxx", (char*)"client_panorama.dll", 4, false, 5};
	}
}