#include <Windows.h>
#include <vector>
#include <TlHelp32.h>
#include <iostream>
#include <sstream>
#include "Hpatterns.h"

struct moduleInfo
{
	DWORD moduleBase;
	DWORD moduleSize;
};

DWORD pID;
HANDLE pHandle;
HANDLE snapshot;
moduleInfo info;

DWORD GetPID() {

	HWND windowHandle = NULL;
	while (windowHandle == NULL)
	{
		windowHandle = FindWindow(0, "Counter-Strike: Global Offensive");
		Sleep(1000);
	}

	DWORD pId;
	GetWindowThreadProcessId(windowHandle, &pId);

	return pId;

}

moduleInfo GetModuleAddressInfo(char *moduleName) {

	MODULEENTRY32 entry;
	entry.dwSize = sizeof(MODULEENTRY32);
	moduleInfo addressInfo;

	if (Module32First(snapshot, &entry)) {
		do {
			if (_stricmp(entry.szModule, moduleName) == 0) {
				addressInfo.moduleBase = (DWORD)entry.modBaseAddr;
				addressInfo.moduleSize = (DWORD)entry.modBaseSize;
				break;
			}
		} while (Module32Next(snapshot, &entry));
	}

	return addressInfo;
}

BOOL ComparePattern(HANDLE pHandle, DWORD address, char *pattern, char *mask) {

	DWORD patternSize = strlen(mask);

	auto memBuf = new char[patternSize + 1];
	memset(memBuf, 0, patternSize + 1);
	ReadProcessMemory(pHandle, (LPVOID)address, memBuf, patternSize, 0);


	for (DWORD i = 1; i < patternSize; i++) {

		if (memBuf[i] != pattern[i] && mask[i] != *"?") {
			delete memBuf;															
			return false;												
		}
	}
	delete memBuf;																	
	return true;																	
}

DWORD ExternalAoBScan(HANDLE pHandle, char *pattern, char *mask, char *module) {
																									
	std::vector<DWORD> matches;																	
	DWORD patternSize = strlen(mask);												

	info = GetModuleAddressInfo(module);

	if (!info.moduleBase || !info.moduleSize) {
		std::cout << "Could not get " << module << " base address or size" << std::endl;
		return NULL;																			
	}

	auto moduleBytes = new char[info.moduleSize + 1];
	memset(moduleBytes, 0, info.moduleSize + 1);
	ReadProcessMemory(pHandle, (LPVOID)info.moduleBase, moduleBytes, info.moduleSize, 0);

	for (int i = 0; i + patternSize < info.moduleSize; i++) {
		if (pattern[0] == moduleBytes[i]) {															
			if (ComparePattern(pHandle, info.moduleBase + i, pattern, mask)) {
				matches.push_back(info.moduleBase + i);
			}
		}
	}

	delete moduleBytes;																			

	if (matches.size() == 0) {																	
		return NULL;																		
	}
	return matches[0];																				
}

DWORD ReadAddress(char *addressName, char *pattern, char *mask, char *module, int skipBytes, bool subtractModule, int bytes2Add)
{
	DWORD address = ExternalAoBScan(pHandle, pattern, mask, module);
	address += skipBytes;
	if (address) {
		DWORD result;
		ReadProcessMemory(pHandle, (PBYTE*)address, &result, sizeof(DWORD), 0);
		if (result)
		{
			if (subtractModule)
			{
				result -= info.moduleBase;
			}
			result += bytes2Add;
			std::cout << addressName << " = " << "0x" << std::hex << result << std::endl;
			return result;
		}
		else
		{
			std::cout << "Can't read the given address" << std::endl;
			return 0;
		}
	}
	else {
		std::cout << "No address" << std::endl;
		return 0;
	}
}

int main() {

	pID = GetPID();
	if (!pID) {
		std::cout << "Could not find process" << std::endl;
		std::cin.get();
		return 1;
	}

	pHandle = OpenProcess(PROCESS_VM_READ, false, pID);
	if (!pHandle) {
		std::cout << "Could not obtain handle" << std::endl;
		std::cin.get();
		return 1;
	}

	snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pID);

	std::cout << "NETVARS:" << std::endl;

	//netvars
	ReadAddress(pussyexplorer::netvars::m_bSpotted.name, pussyexplorer::netvars::m_bSpotted.pattern, pussyexplorer::netvars::m_bSpotted.mask, pussyexplorer::netvars::m_bSpotted.module, pussyexplorer::netvars::m_bSpotted.skipBytes, pussyexplorer::netvars::m_bSpotted.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_dwBoneMatrix.name, pussyexplorer::netvars::m_dwBoneMatrix.pattern, pussyexplorer::netvars::m_dwBoneMatrix.mask, pussyexplorer::netvars::m_dwBoneMatrix.module, pussyexplorer::netvars::m_dwBoneMatrix.skipBytes, pussyexplorer::netvars::m_dwBoneMatrix.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_iCrosshairId.name, pussyexplorer::netvars::m_iCrosshairId.pattern, pussyexplorer::netvars::m_iCrosshairId.mask, pussyexplorer::netvars::m_iCrosshairId.module, pussyexplorer::netvars::m_iCrosshairId.skipBytes, pussyexplorer::netvars::m_iCrosshairId.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_fFlags.name, pussyexplorer::netvars::m_fFlags.pattern, pussyexplorer::netvars::m_fFlags.mask, pussyexplorer::netvars::m_fFlags.module, pussyexplorer::netvars::m_fFlags.skipBytes, pussyexplorer::netvars::m_fFlags.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_iHealth.name, pussyexplorer::netvars::m_iHealth.pattern, pussyexplorer::netvars::m_iHealth.mask, pussyexplorer::netvars::m_iHealth.module, pussyexplorer::netvars::m_iHealth.skipBytes, pussyexplorer::netvars::m_iHealth.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_iTeamNum.name, pussyexplorer::netvars::m_iTeamNum.pattern, pussyexplorer::netvars::m_iTeamNum.mask, pussyexplorer::netvars::m_iTeamNum.module, pussyexplorer::netvars::m_iTeamNum.skipBytes, pussyexplorer::netvars::m_iTeamNum.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_lifeState.name, pussyexplorer::netvars::m_lifeState.pattern, pussyexplorer::netvars::m_lifeState.mask, pussyexplorer::netvars::m_lifeState.module, pussyexplorer::netvars::m_lifeState.skipBytes, pussyexplorer::netvars::m_lifeState.subtractModule, NULL);
	ReadAddress(pussyexplorer::netvars::m_vecOrigin.name, pussyexplorer::netvars::m_vecOrigin.pattern, pussyexplorer::netvars::m_vecOrigin.mask, pussyexplorer::netvars::m_vecOrigin.module, pussyexplorer::netvars::m_vecOrigin.skipBytes, pussyexplorer::netvars::m_vecOrigin.subtractModule, NULL);

	std::cout << std::endl << "SIGNATURES" << std::endl;

	//signatures
	ReadAddress(pussyexplorer::signatures::dwClientState_ViewAngles.name, pussyexplorer::signatures::dwClientState_ViewAngles.pattern, pussyexplorer::signatures::dwClientState_ViewAngles.mask, pussyexplorer::signatures::dwClientState_ViewAngles.module, pussyexplorer::signatures::dwClientState_ViewAngles.skipBytes, pussyexplorer::signatures::dwClientState_ViewAngles.subtractModule, NULL);
	//ReadAddress(pussyexplorer::signatures::dwEntityList.name, pussyexplorer::signatures::dwEntityList.pattern, pussyexplorer::signatures::dwEntityList.mask, pussyexplorer::signatures::dwEntityList.module, pussyexplorer::signatures::dwEntityList.skipBytes, pussyexplorer::signatures::dwEntityList.subtractModule, pussyexplorer::signatures::dwEntityList.bytes2Add);
	//ReadAddress(pussyexplorer::signatures::dwLocalPlayer.name, pussyexplorer::signatures::dwLocalPlayer.pattern, pussyexplorer::signatures::dwLocalPlayer.mask, pussyexplorer::signatures::dwLocalPlayer.module, pussyexplorer::signatures::dwLocalPlayer.skipBytes, pussyexplorer::signatures::dwLocalPlayer.subtractModule, pussyexplorer::signatures::dwLocalPlayer.bytes2Add);
	ReadAddress(pussyexplorer::signatures::m_bDormant.name, pussyexplorer::signatures::m_bDormant.pattern, pussyexplorer::signatures::m_bDormant.mask, pussyexplorer::signatures::m_bDormant.module, pussyexplorer::signatures::m_bDormant.skipBytes, pussyexplorer::signatures::m_bDormant.subtractModule, pussyexplorer::signatures::m_bDormant.bytes2Add);
	
	CloseHandle(snapshot);
	CloseHandle(pHandle);
	std::cin.get();																												
	return 0;																																

}